from flask import Flask, render_template
import sys
import os
import pandas as pd
import json


sys.path.append("/opt/python/current/app/readnextio")
import py.smtp_emailer as smtp_emailer


# print a nice greeting.
def say_hello(username = "World"):
    return '<p>Hello %s!</p>\n' % username

def read_dates():
    try:
        f = open('/tmp/dates', 'r')
        return '<p>%s</p>\n' % f.readlines()
    except:
        return '<p>File Not Found</p>\n'

def draw_chart():
    file = 'py/data/new_cases_05_07_2020.csv' # use datetime.date.today()
    df = pd.read_csv(file)
    chart_data = df.to_dict(orient='records')
    chart_data = json.dumps(chart_data, indent=2)
    data = {'chart_data': chart_data}
    return render_template("new_cases.html", data=data)

def send_email(message="Hey"):
    e = smtp_emailer.SMTPEmailer()
    response = e.send(body_text=message, body_html=f'<p>{message}</p>')
    return f'<p>{response}</p>\n'


# some bits of text for the page.
header_text = '''
    <html>\n<head> <title>EB Flask Test</title> </head>\n<body>'''
instructions = '''
    <p><em>Hint</em>: This is a RESTful web service! Append a username
    to the URL (for example: <code>/Thelonious</code>) to say hello to
    someone specific.</p>\n'''
home_link = '<p><a href="/">Back</a></p>\n'
footer_text = '</body>\n</html>'

# EB looks for an 'application' callable by default.
application = Flask(__name__)

# add a rule for the index page.
application.add_url_rule('/', 'index', (lambda: header_text +
    say_hello() + instructions + footer_text))

# add a rule when the page is accessed with a name appended to the site
# URL.
application.add_url_rule('/<username>', 'hello', (lambda username:
    header_text + say_hello(username) + home_link + footer_text))


application.add_url_rule('/dates', 'dates', (lambda :
    header_text + read_dates() + home_link + footer_text))

application.add_url_rule('/chart', 'chart', (lambda : draw_chart()))

application.add_url_rule('/email/<message>', 'email', (lambda message: header_text + send_email(message) + home_link + footer_text))


app = application

# run the app.
if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production app.
    application.debug = True
    application.run()
