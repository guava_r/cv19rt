import smtplib  
import email.utils
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from dotenv import load_dotenv
import os


# Replace sender@example.com with your "From" address. 
# This address must be verified.
SENDER = 'cuttackgaurav@gmail.com'  
SENDERNAME = 'MishBot'

# Replace recipient@example.com with a "To" address. If your account 
# is still in the sandbox, this address must be verified.
RECIPIENT  = 'cuttackgaurav@gmail.com'


PORT = 587
REGION = 'us-east-2'

# The subject line of the email.
SUBJECT = 'Amazon SES Test (Python smtplib)'

# The email body for recipients with non-HTML email clients.
BODY_TEXT = ("Amazon SES Test\r\n"
             "This email was sent through the Amazon SES SMTP "
             "Interface using the Python smtplib package."
            )

# The HTML body of the email.
BODY_HTML = """<html>
<head></head>
<body>
  <h1>Amazon SES SMTP Email Test</h1>
  <p>This email was sent with Amazon SES using the
    <a href='https://www.python.org/'>Python</a>
    <a href='https://docs.python.org/3/library/smtplib.html'>
    smtplib</a> library.</p>
</body>
</html>
            """


class SMTPEmailer(object):
  def __init__(self, smtp_username=None, smtp_password=None, region=REGION):
    if not smtp_username or not smtp_password:
      load_dotenv()
      self._smtp_username = os.environ.get("smtp_username")
      self._smtp_password = os.environ.get("smtp_password")

    self._host = "email-smtp." + region + ".amazonaws.com"
    self._port = PORT
    

  def send(
    self,
    sender=SENDER,
    sender_name=SENDERNAME,
    recipient=RECIPIENT,
    subject=SUBJECT,
    body_text=BODY_TEXT,
    body_html=BODY_HTML):
    
    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = email.utils.formataddr((sender_name, sender))
    msg['To'] = recipient
    # Comment or delete the next line if you are not using a configuration set
    # msg.add_header('X-SES-CONFIGURATION-SET',CONFIGURATION_SET)

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(body_text, 'plain')
    part2 = MIMEText(body_html, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)
      
    print('username' + self._smtp_username)
    print('password' + self._smtp_password)
    # Try to send the message.
    try:  
        server = smtplib.SMTP(self._host, self._port)
        server.ehlo()
        server.starttls()
        #stmplib docs recommend calling ehlo() before & after starttls()
        server.ehlo()
        server.login(self._smtp_username, self._smtp_password)
        server.sendmail(sender, recipient, msg.as_string())
        server.close()
    # Display an error message if something goes wrong.
    except Exception as e:
        return  str(e)
    else:
        return "Email sent!"