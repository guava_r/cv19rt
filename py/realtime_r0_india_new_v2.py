# -*- coding: utf-8 -*-
"""Realtime R0-India_new-v2.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1lgn-JM_Fzcf2iKsYgdigOzpWeHWJJlNQ

# Estimating COVID-19's $R_t$ in Real-Time
Nidhi Gupta - April 17

Thanks to Kevin Systrom!

In any epidemic, $R_t$ is the measure known as the effective reproduction number. It's the number of people who become infected per infectious person at time $t$. The most well-known version of this number is the basic reproduction number: $R_0$ when $t=0$. However, $R_0$ is a single measure that does not adapt with changes in behavior and restrictions.

As a pandemic evolves, increasing restrictions (or potential releasing of restrictions) changes $R_t$. Knowing the current $R_t$ is essential. When $R\gg1$, the pandemic will spread through a large part of the population. If $R_t<1$, the pandemic will slow quickly before it has a chance to infect many people. The lower the $R_t$: the more manageable the situation. In general, any $R_t<1$ means things are under control.

The value of $R_t$ helps us in two ways. (1) It helps us understand how effective our measures have been controlling an outbreak and (2) it gives us vital information about whether we should increase or reduce restrictions based on our competing goals of economic prosperity and human safety. [Well-respected epidemiologists argue](https://www.nytimes.com/2020/04/06/opinion/coronavirus-end-social-distancing.html) that tracking $R_t$ is the only way to manage through this crisis.

Yet, today, we don't yet use $R_t$ in this way. In fact, the only real-time measure I've seen has been for [Hong Kong](https://covid19.sph.hku.hk/dashboard). More importantly, it is not useful to understand $R_t$ at a national level. Instead, to manage this crisis effectively, we need a local (state, county and/or city) granularity of $R_t$.

What follows is a solution to this problem at the Indian State level. It's a modified version of a solution created by [Bettencourt & Ribeiro 2008](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0002185) to estimate real-time $R_t$ using a Bayesian approach. While this paper estimates a static $R$ value, here we introduce a process model with Gaussian noise to estimate a time-varying $R_t$.

If it's not entirely clear, I'm not an epidemiologist. At the same time, data is data, and statistics are statistics and this is based on work by well-known epidemiologists so you can calibrate your beliefs as you wish. In the meantime, I hope you can learn something new as I did by reading through this example. Feel free to take this work and apply it elsewhere – internationally or to counties in India.
"""

# Commented out IPython magic to ensure Python compatibility.
import pandas as pd
import numpy as np

from matplotlib import pyplot as plt
from matplotlib.dates import date2num, num2date
from matplotlib import dates as mdates
from matplotlib import ticker
from matplotlib.colors import ListedColormap
from matplotlib.patches import Patch

from scipy import stats as sps
from scipy.interpolate import interp1d

# from IPython.display import clear_output

FILTERED_REGIONS = [
    'Virgin Islands',
    'American Samoa',
    'Northern Mariana Islands',
    'Guam',
    'Puerto Rico', 'Ladakh']

FILTERED_REGION_CODES = ['AS1', 'GU1', 'PR1', 'VI1', 'MP1', 'Ladakh']

# %config InlineBackend.figure_format = 'retina'

"""## Bettencourt & Ribeiro's Approach

Every day, we learn how many more people have COVID-19. This new case count gives us a clue about the current value of $R_t$. We also, figure that the value of $R_t$ today is related to the value of $R_{t-1}$ (yesterday's value) and every previous value of $R_{t-m}$ for that matter.

With these insights, the authors use [Bayes' rule](https://en.wikipedia.org/wiki/Bayes%27_theorem) to update their beliefs about the true value of $R_t$ based on how many new cases have been reported each day.

This is Bayes' Theorem as we'll use it:

$$ P(R_t|k)=\frac{P(k|R_t)\cdot P(R_t)}{P(k)} $$

This says that, having seen $k$ new cases, we believe the distribution of $R_t$ is equal to:

- The __likelihood__ of seeing $k$ new cases given $R_t$ times ...
- The __prior__ beliefs of the value of $P(R_t)$ without the data ...
- divided by the probability of seeing this many cases in general.

This is for a single day. To make it iterative: every day that passes, we use yesterday's prior $P(R_{t-1})$ to estimate today's prior $P(R_t)$. We will assume the distribution of $R_t$ to be a Gaussian centered around $R_{t-1}$, so $P(R_t|R_{t-1})=\mathcal{N}(R_{t-1}, \sigma)$, where $\sigma$ is a hyperparameter (see below on how we estimate $\sigma$). So on day one:

$$ P(R_1|k_1) \propto P(R_1)\cdot \mathcal{L}(R_1|k_1)$$

On day two:

$$ P(R_2|k_1,k_2) \propto P(R_2)\cdot \mathcal{L}(R_2|k_2) = \sum_{R_1} {P(R_1|k_1)\cdot P(R_2|R_1)\cdot\mathcal{L}(R_2|k_2) }$$

etc.

### Choosing a Likelihood Function $P\left(k_t|R_t\right)$

A likelihood function function says how likely we are to see $k$ new cases, given a value of $R_t$.

Any time you need to model 'arrivals' over some time period of time, statisticians like to use the [Poisson Distribution](https://en.wikipedia.org/wiki/Poisson_distribution). Given an average arrival rate of $\lambda$ new cases per day, the probability of seeing $k$ new cases is distributed according to the Poisson distribution:

$$P(k|\lambda) = \frac{\lambda^k e^{-\lambda}}{k!}$$
"""

# Column vector of k
k = np.arange(0, 70)[:, None]

# Different values of Lambda
lambdas = [10, 20, 30, 40]

# Evaluated the Probability Mass Function (remember: poisson is discrete)
y = sps.poisson.pmf(k, lambdas)

# Show the resulting shape
print(y.shape)

plt.plot(y)
plt.show()

"""> __Note__: this was a terse expression which makes it tricky. All I did was to make $k$ a column. By giving it a column for $k$ and a 'row' for lambda it will evaluate the pmf over both and produce an array that has $k$ rows and lambda columns. This is an efficient way of producing many distributions all at once, and __you will see it used again below__!"""

fig, ax = plt.subplots()

ax.set(title='Poisson Distribution of Cases\n $P(k|\lambda)$')

plt.plot(k, y,
         marker='o',
         markersize=3,
         lw=0)

plt.legend(title="$\lambda$", labels=lambdas);

"""The Poisson distribution says that if you think you're going to have $\lambda$ cases per day, you'll probably get that many, plus or minus some variation based on chance.

But in our case, we know there have been $k$ cases and we need to know what value of $\lambda$ is most likely. In order to do this, we fix $k$ in place while varying $\lambda$. __This is called the likelihood function.__

For example, imagine we observe $k=20$ new cases, and we want to know how likely each $\lambda$ is:
"""

k = 20

lam = np.linspace(1, 45, 90)

likelihood = pd.Series(data=sps.poisson.pmf(k, lam),
                       index=pd.Index(lam, name='$\lambda$'),
                       name='lambda')

likelihood.plot(title=r'Likelihood $P\left(k_t=20|\lambda\right)$');

"""This says that if we see 20 cases, the most likely value of $\lambda$ is (not surprisingly) 20. But we're not certain: it's possible lambda was 21 or 17 and saw 20 new cases by chance alone. It also says that it's unlikely $\lambda$ was 40 and we saw 20.

Great. We have $P\left(\lambda_t|k_t\right)$ which is parameterized by $\lambda$ but we were looking for $P\left(k_t|R_t\right)$ which is parameterized by $R_t$. We need to know the relationship between $\lambda$ and $R_t$

### Connecting $\lambda$ and $R_t$

__The key insight to making this work is to realize there's a connection between $R_t$ and $\lambda$__. [The derivation](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0002185) is beyond the scope of this notebook, but here it is:

$$ \lambda = k_{t-1}e^{\gamma(R_t-1)}$$

where $\gamma$ is the reciprocal of the serial interval ([about 7 days for COVID19](https://wwwnc.cdc.gov/eid/article/26/7/20-0282_article)). Since we know every new case count on the previous day, we can now reformulate the likelihood function as a Poisson parameterized by fixing $k$ and varying $R_t$.

$$ \lambda = k_{t-1}e^{\gamma(R_t-1)}$$

$$P\left(k|R_t\right) = \frac{\lambda^k e^{-\lambda}}{k!}$$

### Evaluating the Likelihood Function

To continue our example, let's imagine a sample of new case counts $k$. What is the likelihood of different values of $R_t$ on each of those days?
"""

# Assume k new cases, where k can be any number from below array, let's look at what values of Rt we get
k = np.array([20, 40, 55, 90])

# We create an array for every possible value of Rt. We have taken Rt max to be 12. Rt should not go as high as 12
# And created a range of Rt values between 0 to 12
R_T_MAX = 12
r_t_range = np.linspace(0, R_T_MAX, R_T_MAX*100+1)

# Gamma is 1/serial interval
# 7 is number of days in which the cases double 
# https://wwwnc.cdc.gov/eid/article/26/7/20-0282_article
# https://www.nejm.org/doi/full/10.1056/NEJMoa2001316
GAMMA = 1/7

# Map Rt into lambda so we can substitute it into the equation below
# So the lambda equation was only to obtain the relation between Rt and lambda
# So that we get values for lambda for the poisson distribution. 
# Note that we have N-1 lambdas because on the first day of an outbreak you do not know what to expect.
lam = k[:-1] * np.exp(GAMMA * (r_t_range[:, None] - 1))

# Evaluate the likelihood on each day and normalize sum of each day to 1.0
likelihood_r_t = sps.poisson.pmf(k[1:], lam)
likelihood_r_t /= np.sum(likelihood_r_t, axis=0)

# Plot it
ax = pd.DataFrame(
    data = likelihood_r_t,
    index = r_t_range
).plot(
    title='Likelihood of $R_t$ given $k$',
    xlim=(0,10)
)

ax.legend(labels=k[1:], title='New Cases')
ax.set_xlabel('$R_t$');

"""You can see that each day we have a independent guesses for $R_t$. The goal is to combine the information we have about previous days with the current day. To do this, we use Bayes' theorem.

### Performing the Bayesian Update

To perform the Bayesian update, we need to multiply the likelihood by the prior (which is just the previous day's likelihood without our Gaussian update) to get the posteriors. Let's do that using the cumulative product of each successive day:
"""

posteriors = likelihood_r_t.cumprod(axis=1)
posteriors = posteriors / np.sum(posteriors, axis=0)

columns = pd.Index(range(1, posteriors.shape[1]+1), name='Day')

posteriors = pd.DataFrame(
    data = posteriors,
    index = r_t_range,
    columns = columns)

posteriors

posteriors = likelihood_r_t.cumprod(axis=1)
posteriors = posteriors / np.sum(posteriors, axis=0)

columns = pd.Index(range(1, posteriors.shape[1]+1), name='Day')
posteriors = pd.DataFrame(
    data = posteriors,
    index = r_t_range,
    columns = columns)

ax = posteriors.plot(
    title='Posterior $P(R_t|k)$',
    xlim=(0,10)
)
ax.legend(title='Day')
ax.set_xlabel('$R_t$');

"""Notice how on Day 1, our posterior matches Day 1's likelihood from above? That's because we have no information other than that day. However, when we update the prior using Day 2's information, you can see the curve has moved left, but not nearly as left as the likelihood for Day 2 from above. This is because Bayesian updating uses information from both days and effectively averages the two. Since Day 3's likelihood is in between the other two, you see a small shift to the right, but more importantly: a narrower distribution. We're becoming __more__ confident in our believes of the true value of $R_t$.

From these posteriors, we can answer important questions such as "What is the most likely value of $R_t$ each day?"
"""

most_likely_values = posteriors.idxmax(axis=0)
most_likely_values

"""We can also obtain the [highest density intervals](https://www.sciencedirect.com/topics/mathematics/highest-density-interval) for $R_t$:"""

def highest_density_interval(pmf, p=.9):
    # If we pass a DataFrame, just call this recursively on the columns
    if(isinstance(pmf, pd.DataFrame)):
        return pd.DataFrame([highest_density_interval(pmf[col], p=p) for col in pmf],
                            index=pmf.columns)
    
    cumsum = np.cumsum(pmf.values)
    best = None
    for i, value in enumerate(cumsum):
        for j, high_value in enumerate(cumsum[i+1:]):
            if (high_value-value > p) and (not best or j<best[1]-best[0]):
                best = (i, i+j+1)
                break
            
    low = pmf.index[best[0]]
    high = pmf.index[best[1]]
    return pd.Series([low, high], index=[f'Low_{p*100:.0f}', f'High_{p*100:.0f}'])

hdi = highest_density_interval(posteriors)
hdi.tail()

"""Finally, we can plot both the most likely values for $R_t$ and the HDIs over time. This is the most useful representation as it shows how our beliefs change with every day."""

ax = most_likely_values.plot(marker='o',
                             label='Most Likely',
                             title=f'$R_t$ by day',
                             c='k',
                             markersize=4)

ax.fill_between(hdi.index,
                hdi['Low_90'],
                hdi['High_90'],
                color='k',
                alpha=.1,
                lw=0,
                label='HDI')

ax.legend();

"""We can see that the most likely value of $R_t$ changes with time and the highest-density interval narrows as we become more sure of the true value of $R_t$ over time. Note that since we only had four days of history, I did not apply the process to this sample. Next, however, we'll turn to a real-world application where this process is necessary.

# Real-World Application to Indian Data

### Setup

Load Indian state case data from covid19india.org
"""

url = 'http://api.covid19india.org/states_daily_csv/confirmed.csv'
states00 = pd.read_csv(url, parse_dates=['date'])
states00.tail()

states0 = states00.drop(['Unnamed: 40'], axis=1)

states0.rename(columns = {'TT':'India', 'AN':'Andaman and Nicobar Islands'
                                   , 'AP':'Andhra Pradesh', 'AR':'Arunachal Pradesh', 'AS':'Assam', 'BR':'Bihar'
                                   , 'CH':'Chandigarh', 'CT':'Chhattisgarh', 'DN':'Dadra and Nagar Haveli'
                                   , 'DD':'Daman and Diu', 'DL':'Delhi', 'GA':'Goa', 'GJ':'Gujarat'
                                   , 'HR':'Haryana', 'HP':'Himachal Pradesh', 'JK':'Jammu and Kashmir'
                                   , 'JH':'Jharkhand', 'KA':'Karnataka', 'KL':'Kerala'
                                   , 'LA':'Ladakh', 'LD':'Lakshadweep', 'MP':'Madhya Pradesh', 'MH':'Maharashtra'
                                   , 'MN':'Manipur', 'ML':'Meghalaya', 'MZ':'Mizoram'
                                   , 'NL':'Nagaland', 'OR':'Odisha', 'PY':'Puducherry', 'PB':'Punjab'
                                   , 'RJ':'Rajasthan', 'SK':'Sikkim', 'TN':'Tamil Nadu', 'TG':'Telangana'
                                   , 'TR':'Tripura', 'UP':'Uttar Pradesh'
                                   , 'UT':'Uttarakhand', 'WB':'West Bengal'}, inplace = True)

# wide to long format
states1 = states0.melt(id_vars='date', var_name='state')
states1 = states1.rename(columns={'date': 'date', 'state': 'state' , 'value' : 'positive'})
states1 = states1[['state', 'date', 'positive']]
states1['positive'] = states1['positive'].fillna(0)
states1.positive = states1.positive.astype(int)
print(states1.isnull().sum())
states1.tail()

states1[states1.positive<0]

states1.loc[(states1['state'] == 'Punjab') & (states1['date']== '2020-05-15'), 'positive'] = 0
states1.loc[(states1['state'] == 'Tripura') & (states1['date']== '2020-05-17'), 'positive'] = 0
states1[states1.positive<0]

# groupby and remove states with no cases
states2 = states1.groupby(['state']).sum().reset_index()

states3 = states2[states2.positive>=5]

states4 = states1[states1['state'].isin(states3['state'])]
states4

states4 = states4[states4['state']!='UN']

states4.to_csv('states2.csv')

states = pd.read_csv('states2.csv',
                     usecols=['date', 'state', 'positive'],
                     parse_dates=['date'],
                     index_col=['state', 'date'],
                     squeeze=True).sort_index()
states.tail()

"""Taking a look at the state, we need to start the analysis when there are a consistent number of cases each day. Find the last zero new case day and start on the day after that.

Also, case reporting is very erratic based on testing backlogs, etc. To get the best view of the 'true' data we can, I've applied a gaussian filter to the time series. This is obviously an arbitrary choice, but you'd imagine the real world process is not nearly as stochastic as the actual reporting.
"""

# g = ['Assam','Tripura','Manipur','Himachal Pradesh','Uttarakhand','Puducherry','c','Goa','Meghalaya','Andaman and Nicobar Islands']

states4[states4.state=='Manipur'].sum()

state_name = 'India'

def prepare_cases(cases):

    smoothed = cases.rolling(9,
        win_type='gaussian',
        min_periods=1,
        center=True).mean(std=3).round()
    
    zeros = smoothed.index[smoothed.eq(0)]
    if len(zeros) == 0:
        idx_start = 0
    else:
        last_zero = zeros.max()
        idx_start = smoothed.index.get_loc(last_zero) + 1
    smoothed = smoothed.iloc[idx_start:]
    original = cases.loc[smoothed.index]
    
    return original, smoothed

cases = states.xs(state_name).rename(f"{state_name} cases")
# cases = cases.loc[cases[(cases != 0)].first_valid_index():].rename(f"{state_name} cases")

original, smoothed = prepare_cases(cases)

original.plot(title=f"{state_name} New Cases per Day",
               c='k',
               linestyle=':',
               alpha=.5,
               label='Actual',
               legend=True,
             figsize=(600/72, 400/72))

ax = smoothed.plot(label='Smoothed', color ='blue', alpha=5,
                   legend=True)

ax.get_figure().set_facecolor('w')

# plt.savefig(f'{state_name} New Cases per Day.png')

# R_T_MAX = 12
# r_t_range = np.linspace(0, R_T_MAX, R_T_MAX*100+1)
# prior0 = sps.gamma(a=4).pdf(r_t_range)
# prior0 /= prior0.sum()
# prior0

len(r_t_range)

r_t_range[:, None] - 1

states['India'][-9:].mean()

smoothed

smoothed[:-1]

# lam = smoothed[:-1].values * np.exp(GAMMA * (r_t_range[:, None] - 1))
# lam[-2:]

# data = sps.poisson.pmf(smoothed[1:].values, lam)
# data[-1:]

#  likelihoods = pd.DataFrame(
#         data = sps.poisson.pmf(smoothed[1:].values, lam),
#         index = r_t_range,
#         columns = smoothed.index[1:])
# likelihoods

def get_posteriors(sr, sigma=0.15):

    # (1) Calculate Lambda
    lam = sr[:-1].values * np.exp(GAMMA * (r_t_range[:, None] - 1))

    
    # (2) Calculate each day's likelihood
    likelihoods = pd.DataFrame(
        data = sps.poisson.pmf(sr[1:].values, lam),
        index = r_t_range,
        columns = sr.index[1:])
    
    # (3) Create the Gaussian Matrix
    process_matrix = sps.norm(loc=r_t_range,
                              scale=sigma
                             ).pdf(r_t_range[:, None]) 

    # (3a) Normalize all rows to sum to 1
    process_matrix /= process_matrix.sum(axis=0)
    
    # (4) Calculate the initial prior
    prior0 = sps.gamma(a=4).pdf(r_t_range)
    prior0 /= prior0.sum()

    # Create a DataFrame that will hold our posteriors for each day
    # Insert our prior as the first posterior.
    posteriors = pd.DataFrame(
        index=r_t_range,
        columns=sr.index,
        data={sr.index[0]: prior0}
    )
    
    # We said we'd keep track of the sum of the log of the probability
    # of the data for maximum likelihood calculation.
    log_likelihood = 0.0

    # (5) Iteratively apply Bayes' rule
    for previous_day, current_day in zip(sr.index[:-1], sr.index[1:]):

        #(5a) Calculate the new prior
        current_prior = process_matrix @ posteriors[previous_day]
        
        #(5b) Calculate the numerator of Bayes' Rule: P(k|R_t)P(R_t)
        numerator = likelihoods[current_day] * current_prior
        
        #(5c) Calcluate the denominator of Bayes' Rule P(k)
        denominator = np.sum(numerator)
        
        # Execute full Bayes' Rule
        posteriors[current_day] = numerator/denominator
        
        # Add to the running sum of log likelihoods
        log_likelihood += np.log(denominator)
    
    return posteriors, log_likelihood

# Note that we're fixing sigma to a value just for the example
posteriors, log_likelihood = get_posteriors(smoothed, sigma=.25)

"""### The Result

Below you can see every day (row) of the posterior distribution plotted simultaneously. The posteriors start without much confidence (wide) and become progressively more confident (narrower) about the true value of $R_t$
"""

ax = posteriors.plot(title=f'{state_name} - Daily Posterior for $R_t$',
           legend=False, 
           lw=1,
           c='k',
           alpha=.3,
           xlim=(0.4,6))

ax.set_xlabel('$R_t$');

"""### Plotting in the Time Domain with Credible Intervals

Since our results include uncertainty, we'd like to be able to view the most likely value of $R_t$ along with its highest-density interval.
"""

posteriors

# Note that this takes a while to execute - it's not the most efficient algorithm
hdis = highest_density_interval(posteriors, p=.9)

most_likely = posteriors.idxmax().rename('ML')

# Look into why you shift -1
result = pd.concat([most_likely, hdis], axis=1)

result.tail()

def plot_rt(result, ax, state_name):
    
    ax.set_title(f"{state_name}")
    
    # Colors
    ABOVE = [1,0,0]
    MIDDLE = [1,1,1]
    BELOW = [0,0,0]
    cmap = ListedColormap(np.r_[
        np.linspace(BELOW,MIDDLE,25),
        np.linspace(MIDDLE,ABOVE,25)
    ])
    color_mapped = lambda y: np.clip(y, .5, 1.5)-.5
    
    index = result['ML'].index.get_level_values('date')
    values = result['ML'].values
    
    # Plot dots and line
    ax.plot(index, values, c='k', zorder=1, alpha=.25)
    ax.scatter(index,
               values,
               s=40,
               lw=.5,
               c=cmap(color_mapped(values)),
               edgecolors='k', zorder=2)
    
    # Aesthetically, extrapolate credible interval by 1 day either side
    lowfn = interp1d(date2num(index),
                     result['Low_90'].values,
                     bounds_error=False,
                     fill_value='extrapolate')
    
    highfn = interp1d(date2num(index),
                      result['High_90'].values,
                      bounds_error=False,
                      fill_value='extrapolate')
    
    extended = pd.date_range(start=pd.Timestamp('2020-03-13'),
                             end=index[-1]+pd.Timedelta(days=1))
    
    ax.fill_between(extended,
                    lowfn(date2num(extended)),
                    highfn(date2num(extended)),
                    color='green',
                    alpha=.1,
                    lw=0,
                    zorder=3)

    ax.axhline(1.0, c='k', lw=1, label='$R_t=1.0$', alpha=.25);
    
    # Formatting
    ax.xaxis.set_major_locator(mdates.MonthLocator())
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b'))
    ax.xaxis.set_minor_locator(mdates.DayLocator())
    
    ax.yaxis.set_major_locator(ticker.MultipleLocator(.5))
    ax.yaxis.set_major_formatter(ticker.StrMethodFormatter("{x:.1f}"))
    ax.yaxis.tick_right()
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.margins(0)
    ax.grid(which='major', axis='y', c='k', alpha=.1, zorder=-2)
    ax.margins(0)
    ax.set_ylim(0.0, 4.0)
    ax.set_xlim(pd.Timestamp('2020-03-13'), result.index.get_level_values('date')[-1]+pd.Timedelta(days=1))
    fig.set_facecolor('w')

    
fig, ax = plt.subplots(figsize=(600/72,400/72))

plot_rt(result, ax, state_name)
ax.set_title(f'Real-time $R_t$ for {state_name}')
ax.xaxis.set_major_locator(mdates.WeekdayLocator())
ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %d'))
#plt.savefig(f'Real-time $R_t$ for {state_name}.png')

"""### Choosing the optimal $\sigma$

In the previous section we described choosing an optimal $\sigma$, but we just assumed a value. But now that we can evaluate each state with any sigma, we have the tools for choosing the optimal $\sigma$.

Above we said we'd choose the value of $\sigma$ that maximizes the likelihood of the data $P(k)$. Since we don't want to overfit on any one state, we choose the sigma that maximizes $P(k)$ over every state. To do this, we add up all the log likelihoods per state for each value of sigma then choose the maximum.

> Note: this takes a while!
"""

sigmas = np.linspace(1/20, 1, 20)

targets = ~states.index.get_level_values('state').isin(FILTERED_REGION_CODES)
states_to_process = states.loc[targets]
# states_to_process = states_to_process.loc[states_to_process[(states_to_process != 0)].first_valid_index():]

results = {}
skip_state_name = {}

for state_name, cases in states_to_process.groupby(level='state'):
    
    print(state_name)
    new, smoothed = prepare_cases(cases)

    result = {}
    
    # Holds all posteriors with every given value of sigma
    result['posteriors'] = []
    
    # Holds the log likelihood across all k for each value of sigma
    result['log_likelihoods'] = []
    
    
    if len(smoothed)>0:
        for sigma in sigmas:
            posteriors, log_likelihood = get_posteriors(smoothed, sigma=sigma)
            result['posteriors'].append(posteriors)
            result['log_likelihoods'].append(log_likelihood)
    else:
        skip_state_name[state_name] = state_name
    
    # Store all results keyed off of state name
    results[state_name] = result
    # clear_output(wait=True)

print('Done.')

"""Now that we have all the log likelihoods, we can sum for each value of sigma across states, graph it, then choose the maximum."""

# Each index of this array holds the total of the log likelihoods for
# the corresponding index of the sigmas array.
total_log_likelihoods = np.zeros_like(sigmas)

# Loop through each state's results and add the log likelihoods to the running total.
for state_name, result in results.items():
    if state_name in skip_state_name:
        print(state_name)
    else:
        total_log_likelihoods += result['log_likelihoods']

# Select the index with the largest log likelihood total
max_likelihood_index = total_log_likelihoods.argmax()

# Select the value that has the highest log likelihood
sigma = sigmas[max_likelihood_index]

# Plot it
fig, ax = plt.subplots()
ax.set_title(f"Maximum Likelihood value for $\sigma$ = {sigma:.2f}");
ax.plot(sigmas, total_log_likelihoods)
ax.axvline(sigma, color='k', linestyle=":")

"""### Compile Final Results

Given that we've selected the optimal $\sigma$, let's grab the precalculated posterior corresponding to that value of $\sigma$ for each state. Let's also calculate the 90% and 50% highest density intervals (this takes a little while) and also the most likely value.
"""

final_results = None

for state_name, result in results.items():
    if state_name in skip_state_name:
        print(state_name)
    else:
        print(state_name)
        posteriors = result['posteriors'][max_likelihood_index]
        hdis_90 = highest_density_interval(posteriors, p=.9)
        hdis_50 = highest_density_interval(posteriors, p=.5)
        most_likely = posteriors.idxmax().rename('ML')
        result = pd.concat([most_likely, hdis_90, hdis_50], axis=1)
        if final_results is None:
            final_results = result
        else:
            final_results = pd.concat([final_results, result])
        # clear_output(wait=True)

print('Done.')

"""### Plot All Indian States"""

for i, (state_name, result) in enumerate(final_results.groupby('state')):
    if state_name in skip_state_name:
        print(state_name)
    else:
        fig, axes = plt.subplots(figsize=(600/72,400/72))
        plot_rt(result, axes, state_name)
        axes.xaxis.set_major_locator(mdates.WeekdayLocator())
        axes.xaxis.set_major_formatter(mdates.DateFormatter('%b %d'))
        axes.set_title(f'Real-time $R_t$ for {state_name}')
#         plt.savefig(f'Real-time $R_t$ for {state_name}.png')
        
fig.tight_layout()
fig.set_facecolor('w')

"""### Export Data to CSV"""

# Uncomment the following line if you'd like to export the data
# final_results.to_csv('data_rt.csv')

"""### Standings"""

# As of 4/12
no_lockdown = [
    'North Dakota', 'ND',
    'South Dakota', 'SD',
    'Nebraska', 'NB',
    'Iowa', 'IA',
    'Arkansas','AR'
]
partial_lockdown = [
    'Utah', 'UT',
    'Wyoming', 'WY',
    'Oklahoma', 'OK',
    'Massachusetts', 'MA'
]

FULL_COLOR = [.7,.7,.7]
NONE_COLOR = [133/255,224/255,133/255]
PARTIAL_COLOR = [.5,.5,.5]
ERROR_BAR_COLOR = [.3,.3,.3]

final_results

filtered = final_results.index.get_level_values(0).isin(FILTERED_REGIONS)
mr = final_results.loc[~filtered].groupby(level=0)[['ML', 'High_90', 'Low_90']].last()

def plot_standings(mr, figsize=None, title='Most Recent $R_t$ by State'):
    if not figsize:
        figsize = ((25/30)*len(mr)+.1,5)
        
    fig, ax = plt.subplots(figsize=figsize)

    ax.set_title(title)
    err = mr[['Low_90', 'High_90']].sub(mr['ML'], axis=0).abs()
    bars = ax.bar(mr.index,
                  mr['ML'],
                  width=.825,
                  color=NONE_COLOR,
                  ecolor=ERROR_BAR_COLOR,
                  capsize=2,
                  error_kw={'alpha':.5, 'lw':1},
                  yerr=err.values.T)

    for bar, state_name in zip(bars, mr.index):
        if state_name in no_lockdown:
            bar.set_color(NONE_COLOR)
        if state_name in partial_lockdown:
            bar.set_color(PARTIAL_COLOR)

    labels = mr.index.to_series().replace({'Andaman and Nicobar Islands':'A & N Islands'}).replace({'Jammu and Kashmir':'Jammu & Kashmir'})
    ax.set_xticklabels(labels, rotation=90, fontsize=13)
    ax.margins(0)
    ax.set_ylim(0,5.)
    ax.axhline(1.0, linestyle=':', color='k', lw=1)

#     leg = ax.legend(handles=[
#                         Patch(label='Full', color=FULL_COLOR),
#                         Patch(label='Partial', color=PARTIAL_COLOR),
#                         Patch(label='None', color=NONE_COLOR)
#                     ],
#                     title='Lockdown',
#                     ncol=3,
#                     loc='upper left',
#                     columnspacing=.75,
#                     handletextpad=.5,
#                     handlelength=1)

#     leg._legend_box.align = "left"
    fig.set_facecolor('w')
    return fig, ax

mr.sort_values('ML', inplace=True)
plot_standings(mr);
plt.tight_layout()
#plt.savefig(f'Most Recent $R_t$ by State.png', dpi=199)

mr.sort_values('Low_90', inplace=True)
plot_standings(mr);

show = mr[mr.High_90.le(1)].sort_values('ML')
fig, ax = plot_standings(show, title='Likely Under Control');

show = mr[mr.Low_90.ge(1.0)].sort_values('Low_90')
fig, ax = plot_standings(show, title='Likely Not Under Control');

# initialize list of lists 
data = [['India','India'], ['AN','Andaman and Nicobar Islands'], ['AP','Andhra Pradesh'],
       ['AR','Arunachal Pradesh'], ['AS','Assam'], ['BR','Bihar'],
       ['CH','Chandigarh'], ['CT', 'Chhattisgarh'], ['DN','Dadra and Nagar Haveli'],
       ['DD','Daman and Diu'], ['DL','Delhi'], ['GA','Goa'],
       ['GJ','Gujarat'], ['HR','Haryana'], ['HP','Himachal Pradesh'],
       ['JK','Jammu and Kashmir'], ['JH','Jharkhand'], ['KA','Karnataka'],
       ['KL','Kerala'], ['LA','Ladakh'], ['LD','Lakshadweep'],
       ['MP','Madhya Pradesh'], ['MH','Maharashtra'], ['MN','Manipur'],
       ['ML','Meghalaya'], ['MZ','Mizoram'], ['NL','Nagaland'],
       ['OR','Odisha'], ['PY','Puducherry'], ['PB','Punjab'],
       ['RJ','Rajasthan'], ['SK','Sikkim'], ['TN','Tamil Nadu'],
       ['TG','Telangana'], ['TR','Tripura'], ['UP','Uttar Pradesh'],
       ['UT','Uttarakhand'], ['WB','West Bengal']
       ] 
  
# Create the pandas DataFrame 
df = pd.DataFrame(data, columns = ['state_ab', 'state'])

final_results1 = final_results

final_results1.reset_index(inplace=True)  
final_results1

final_results2 = pd.merge(final_results1, df, how = 'left', left_on='state', right_on = 'state')
final_results2

# make insufficient data flag - 'Uttarakhand',
g = ['Sikkim','Mizoram','Dadra and Nagar Haveli','Arunachal Pradesh','Nagaland','Meghalaya','Andaman and Nicobar Islands','UN']
final_results2['insufficient_data'] = np.where(final_results2['state'].isin(g), '1', '0')
final_results2[final_results2.insufficient_data=='1'].head(50)

final_results2[final_results2['state']=='Meghalaya']

states4[states4['state']=='Meghalaya'].tail(30)

final_results2.to_csv('data_rt_0625.csv')



skip_state_name

#  on terminal in the same directory, cd to this directory , zip -r Rt_States_2104.zip Rt_States_2104

