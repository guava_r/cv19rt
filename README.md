# README #

+ Clone the repo:


```shell
git clone https://guava_r@bitbucket.org/guava_r/cv19rt.git
cd cv19rt
```


+ Add files.

+ Push to repo:


```shell
git add .
git commit -m 'commit message'
git push origin master
```
